﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Web;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;


namespace ecomm.model.repository
{
    public class helper_repository
    {
        public void Loginfo(string strLine)
        {
            try
            {
                string filename = string.Format("BulkLog_" + DateTime.Now.ToString("yyyyMMdd") + ".txt", DateTime.Now.ToShortDateString());
                if (!Directory.Exists(@"C:\EmailLog\"))
                    Directory.CreateDirectory(@"C:\EmailLog\");

                FileStream fs = new FileStream(@"C:\EmailLog\" + filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
                sw.BaseStream.Seek(0, SeekOrigin.End);
                sw.WriteLine(strLine);
                sw.Close();

            }
            catch
            {
                ;
            }

        }


        public int SendMail(string strsmtpclient, string mailFrom, string mailpwd, int port, int secured,
                            string strMailFromDisplay, string strReplyTo, string strReplyDisplay,
                            string toList, string ccList, string bccList, string subject, string body)
        {

            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();

            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(mailFrom, strMailFromDisplay);
                message.From = fromAddress;

                if (strReplyTo.Trim().Length > 0 && strReplyDisplay.Trim().Length > 0)
                    message.ReplyTo = new MailAddress(strReplyTo, strReplyDisplay);

                message.To.Add(toList);

                if (ccList != null && ccList != string.Empty)
                    message.CC.Add(ccList);

                if (bccList != null && bccList != string.Empty)
                    message.Bcc.Add(bccList);


                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;
                // We use gmail as our smtp client
                smtpClient.Host = strsmtpclient;
                smtpClient.Port = port;
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(
                    mailFrom, mailpwd);

                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                Loginfo(DateTime.Now.ToLocalTime().ToString() + " Exception " + toList + " : " + ex.Message);
                return 0;
            }
            return 1;
        }

        public int SendMailWithAttachment(string toList, string ccList, string bccList, string subject, string body, string fileName)
        {

            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string strsmtpclient = System.Configuration.ConfigurationSettings.AppSettings["SmtpClient"].ToString();
            int port = Convert.ToInt16(ConfigurationSettings.AppSettings["SmtpPort"].ToString());
            string mailFrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
            string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();

            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(mailFrom);
                message.From = fromAddress;
                message.To.Add(toList);
                if (ccList != null && ccList != string.Empty)
                    message.CC.Add(ccList);

                if (bccList != null && bccList != string.Empty)
                    message.CC.Add(bccList);

                if (fileName.Trim().Length > 0)
                {
                    message.Attachments.Add(new Attachment(fileName));
                }

                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;
                // We use gmail as our smtp client
                smtpClient.Host = strsmtpclient;
                smtpClient.Port = port;
                smtpClient.EnableSsl = false;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(
                    mailFrom, mailpwd);

                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 1;
        }

    }
}
