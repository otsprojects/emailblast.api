﻿using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
// to be removed
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.model.repository
{
    public class program_repository
    {

        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {

                return DateTime.Parse("1/1/1800");
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private QueryDocument createQueryDocument(string s)
        {
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(s);
            QueryDocument queryDoc = new QueryDocument(document);
            return queryDoc;
        }

        private countercollection getNextSequence(string seq_type, string track_field)
        {
            dal.DataAccess dal = new DataAccess();
            string q = "{'_id':'" + seq_type + "'}";
            QueryDocument queryDoc = createQueryDocument(q);
            UpdateBuilder u = new UpdateBuilder();
            u.Inc(track_field, 1);
            BsonDocument nextorderid = dal.mongo_find_and_modify("counters", queryDoc, u);
            var ordid = nextorderid.ToString();
            countercollection occ = JsonConvert.DeserializeObject<countercollection>(nextorderid.ToString());
            return occ;
        }

        public List<program_master> get_all_program_master(program_master opm)
        {
            try
            {
                List<program_master> olpm = new List<program_master>();

                dal.DataAccess dal = new DataAccess();

                //****************************Get All Master Programs****************************************//        
                string qry = "{}";
                BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qry);
                QueryDocument qDoc = new QueryDocument(doc);
                string[] in_fields = new string[] { "program_id", "program_name", "ref_link", "program_desc", "program_start_dt", "program_end_dt", "active" };
                string[] ex_fields = new string[] { "_id" };
                MongoCursor cursr = dal.execute_mongo_db("program_master", qDoc, in_fields, ex_fields);
                foreach (var c in cursr)
                {
                    program_master pm = new program_master();

                    pm.program_id = check_field(c.ToBsonDocument(), "program_id") ? convertToInt(c.ToBsonDocument()["program_id"]) : 0;
                    pm.program_name = check_field(c.ToBsonDocument(), "program_name") ? convertToString(c.ToBsonDocument()["program_name"]) : null;
                    pm.ref_link = check_field(c.ToBsonDocument(), "ref_link") ? convertToString(c.ToBsonDocument()["ref_link"]) : null;
                    pm.program_desc = check_field(c.ToBsonDocument(), "program_desc") ? convertToString(c.ToBsonDocument()["program_desc"]) : null;
                    pm.program_start_dt = check_field(c.ToBsonDocument(), "program_start_dt") ? convertToDate(c.ToBsonDocument()["program_start_dt"]) : System.DateTime.Now.ToLocalTime();
                    pm.program_end_dt = check_field(c.ToBsonDocument(), "program_end_dt") ? convertToDate(c.ToBsonDocument()["program_end_dt"]) : System.DateTime.Now.ToLocalTime();
                    pm.active = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 0;
                    olpm.Add(pm);
                }

                return olpm;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new List<program_master>();
            }



        }

        public List<sku_master> get_sku_program_rule(program_master opm)
        {
            try
            {
                List<sku_master> olpm = new List<sku_master>();
                dal.DataAccess dal = new DataAccess();

                bool rule_exist = false;

                //****************************Get SKU Program Rule****************************************//        

                string qry2 = "{active : 1}";
                BsonDocument doc2 = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qry2);
                QueryDocument qDoc2 = new QueryDocument(doc2);
                string[] in_fields2 = new string[] { "sku", "active" };
                string[] ex_fields2 = new string[] { "_id" };
                MongoCursor cursr2 = dal.execute_mongo_db("sku_master", qDoc2, in_fields2, ex_fields2);


                //Gettin Already Existing SKU Rule
                string qry = "{'program_id': " + convertToInt(opm.program_id) + " , active : 1}";
                BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qry);
                QueryDocument qDoc = new QueryDocument(doc);
                string[] in_fields = new string[] { "program_id", "program_rule_id", "sku_rule", "active" };
                string[] ex_fields = new string[] { "_id" };
                MongoCursor cursr = dal.execute_mongo_db("program_rules", qDoc, in_fields, ex_fields);

                List<sku_rule> existing_sku_rule = new List<sku_rule>();

                foreach (var c in cursr)
                {
                    existing_sku_rule = JsonConvert.DeserializeObject<List<sku_rule>>(c.ToBsonDocument()["sku_rule"].ToString());
                    break;
                }

                foreach (var c in cursr2)
                {
                    sku_master pm = new sku_master();
                    string cur_sku = "";
                    cur_sku = check_field(c.ToBsonDocument(), "sku") ? convertToString(c.ToBsonDocument()["sku"]) : null;
                    int allot_points = 0;
                    if (existing_sku_rule.Count() == 0)
                    {
                        allot_points = 0;
                    }
                    else
                    {
                        var sku_obj = existing_sku_rule.FindAll(delegate(sku_rule o_sku_rule) { return convertToString(o_sku_rule.sku) == cur_sku; });
                        if (sku_obj.Count() > 0)
                        {
                            allot_points = convertToInt(sku_obj[0].allotted_points);
                        }
                    }
                    if (cur_sku != null && cur_sku != "")
                    {
                        pm.sku = cur_sku;
                        pm.allotted_points = allot_points;
                        pm.active = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 0;
                        pm.description = check_field(c.ToBsonDocument(), "description") ? convertToString(c.ToBsonDocument()["description"]) : null;
                        olpm.Add(pm);
                    }
                }



                return olpm;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new List<sku_master>();
            }



        }

        public string save_sku_program_rule(program_rule opr)
        {
            try
            {
                List<sku_master> olpm = new List<sku_master>();
                dal.DataAccess dal = new DataAccess();

                //****************************Update Status Of Previous Program Rule Related To SKU****************************************//    
                //evoke mongodb connection method in dal
                // create query
                string qry = "{'program_id': " + convertToInt(opr.program_id) + "}";
                QueryDocument queryDoc = createQueryDocument(qry);
                // create update 
                UpdateBuilder u = new UpdateBuilder();
                u.Set("active", 0).Set("deactivate_date", System.DateTime.Now.ToLocalTime());
                // exec
                //string result = dal.mongo_find_and_update("program_rules", queryDoc, u);

                //****************************Insert Program Rule Related To SKU****************************************//    

                program_rule opr_new = new program_rule();
                opr_new._id = ObjectId.GenerateNewId().ToString();
                opr_new.program_id = convertToInt(opr.program_id);

                var program_rule_id_info = getNextSequence("program_rule_id", "seq");
                var nextprogram_rule_id = convertToDouble(program_rule_id_info.seq);
                opr_new.program_rule_id = convertToInt(nextprogram_rule_id);

                opr_new.sku_rule = opr.sku_rule;
                opr_new.inv_date_rule = new List<inv_dt_rule>();
                opr_new.created_by = convertToString(opr.created_by);
                opr_new.created_date = System.DateTime.Now.ToLocalTime();
                opr_new.active = 1;
                BsonDocument bd_sku = opr_new.ToBsonDocument();
                string sku_result = dal.mongo_write("program_rules", bd_sku);

                return sku_result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Error";
            }



        }

        public List<program_rule> get_InvDate_program_rule(program_master opm)
        {
            try
            {
                List<program_rule> olpr = new List<program_rule>();
                dal.DataAccess dal = new DataAccess();

                //Gettin Program Rule
                string qry = "{'program_id': " + convertToInt(opm.program_id) + "}";
                BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qry);
                QueryDocument qDoc = new QueryDocument(doc);
                string[] in_fields = new string[] { "program_id", "program_rule_id", "sku_rule", "inv_date_rule", "active" };
                string[] ex_fields = new string[] { "_id" };
                MongoCursor cursr = dal.execute_mongo_db("program_rules", qDoc, in_fields, ex_fields);

                foreach (var c in cursr)
                {
                    program_rule opr = new program_rule();
                    opr.program_id = check_field(c.ToBsonDocument(), "program_id") ? convertToInt(c.ToBsonDocument()["program_id"]) : 0;
                    opr.inv_date_rule = check_field(c.ToBsonDocument(), "inv_date_rule") ? JsonConvert.DeserializeObject<List<inv_dt_rule>>(convertToString(c.ToBsonDocument()["inv_date_rule"])) : new List<inv_dt_rule>();
                    opr.active = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 0;
                    if (opr.active == 0)
                    {
                        opr.status = "Deactivated";
                    }
                    else
                    {
                        opr.status = "Active";
                    }

                    olpr.Add(opr);

                }

                return olpr;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new List<program_rule>();
            }



        }

        public string save_InvDate_program_rule(program_rule opr)
        {
            try
            {
                List<sku_master> olpm = new List<sku_master>();
                dal.DataAccess dal = new DataAccess();

                //****************************Update Status Of Previous Program Rule Related To SKU****************************************//    
                //evoke mongodb connection method in dal
                // create query
                string qry = "{'program_id': " + convertToInt(opr.program_id) + "}";
                QueryDocument queryDoc = createQueryDocument(qry);
                // create update 
                UpdateBuilder u = new UpdateBuilder();
                u.Set("active", 0).Set("deactivate_date", System.DateTime.Now.ToLocalTime());
                // exec
                //string result = dal.mongo_find_and_update("program_rules", queryDoc, u);

                //****************************Insert Program Rule Related To SKU****************************************//    

                program_rule opr_new = new program_rule();
                opr_new._id = ObjectId.GenerateNewId().ToString();
                opr_new.program_id = convertToInt(opr.program_id);

                var program_rule_id_info = getNextSequence("program_rule_id", "seq");
                var nextprogram_rule_id = convertToDouble(program_rule_id_info.seq);
                opr_new.program_rule_id = convertToInt(nextprogram_rule_id);

                opr_new.sku_rule = new List<sku_rule>();
                opr_new.inv_date_rule = opr.inv_date_rule;
                opr_new.created_by = convertToString(opr.created_by);
                opr_new.created_date = System.DateTime.Now.ToLocalTime();
                opr_new.active = 1;
                BsonDocument bd_sku = opr_new.ToBsonDocument();
                string sku_result = dal.mongo_write("program_rules", bd_sku);

                return sku_result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Error";
            }



        }

    }
}
