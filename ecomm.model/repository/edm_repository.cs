﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
using Newtonsoft.Json;
// to be removed
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System.Data;
using System.Collections;
using System.Configuration;




namespace ecomm.model.repository
{
    public class edm_repository
    {
        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {

                return DateTime.Parse("1/1/1800");
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string store_edm_data(edm eee)
        {

            try
            {

                dal.DataAccess dal = new DataAccess();


                //string result = "";
                //_edm.selected_html = html;
                eee._id = ObjectId.GenerateNewId().ToString();

                BsonDocument edm_dta = eee.ToBsonDocument();
                string sku_result = dal.mongo_write("edm_master", edm_dta);

                return sku_result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Error";
            }


        }

        public admin_users check_user_login(admin_users sysuser)
        {
            try
            {
                string result = "";
                admin_users lgindta = new admin_users();
                dal.DataAccess dal = new DataAccess();
                //Gettin Already Existing Users
                string qry = "{'user_name': '" + convertToString(sysuser.user_name) + "' ,'password': '" + convertToString(sysuser.password) + "'}";
                BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qry);
                QueryDocument qDoc = new QueryDocument(doc);
                string[] in_fields = new string[] { "user_name", "password" };
                string[] ex_fields = new string[] { "_id" };
                MongoCursor cursr = dal.execute_mongo_db("admin_users", qDoc, in_fields, ex_fields);
                bool flag = false;
                foreach (var c in cursr)
                {
                    admin_users lg = new admin_users();
                    lg.user_name = check_field(c.ToBsonDocument(), "user_name") ? convertToString(c.ToBsonDocument()["user_name"]) : null;
                    lg.password = check_field(c.ToBsonDocument(), "password") ? convertToString(c.ToBsonDocument()["password"]) : null;
                    lgindta = lg;
                    flag = true;
                    lg.msg = "Login Successfully";
                    break;
                }

                if (flag == false)
                {
                    result = "Invalid Username Or Password";
                }

                return lgindta;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new admin_users();
            }


        }


        public string save_upload_list(emailusers oeu, string collection_name)
        {

            dal.DataAccess dal = new DataAccess();
            oeu._id = ObjectId.GenerateNewId().ToString();
            BsonDocument list_dta = oeu.ToBsonDocument();
            string upldlst = dal.mongo_write(convertToString(collection_name), list_dta);

            return upldlst;
        }


        public string update_edm_content(string edm_id, string collection_name)
        {

            dal.DataAccess dal = new DataAccess();

            //****************************Update Status Of Previous Program Rule Related To SKU****************************************//    
            //evoke mongodb connection method in dal
            //create query
            string qry = "{'edmid': '" + convertToString(edm_id) + "'}";
            QueryDocument queryDoc = createQueryDocument(qry);
            //// create update 
            UpdateBuilder u = new UpdateBuilder();
            u.Set("user_collection", convertToString(collection_name)).Set("user_col_update", System.DateTime.Now.ToLocalTime());
            //// exec
            BsonDocument result = dal.mongo_find_and_modify("edm_master", queryDoc, u);

            return result.ToString();
        }
        private QueryDocument createQueryDocument(string s)
        {
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(s);
            QueryDocument queryDoc = new QueryDocument(document);
            return queryDoc;
        }


        public string save_upload_cnfgr(config cn)
        {

            dal.DataAccess dal = new DataAccess();

            cn._id = ObjectId.GenerateNewId().ToString();
            cn.created_dt = System.DateTime.Now.ToLocalTime();

            BsonDocument cnfgr_dta = cn.ToBsonDocument();
            string upldcnfgr = dal.mongo_write("ConfigData", cnfgr_dta);

            return upldcnfgr;
        }

        #region For Trigerring Bulk Emails
        public void send_mail(config cn)
        {
            /****Declaring Template Fields****/
            string first_name = "";
            string last_name = "";
            string strLog = "";
            string strTo = "";
            string strSubject = "";
            string srlNo = "";
            string edm_id = "";
            string subject = "";
            bool unChk = false;
            string user_email = "";//To hold Unsubscribe Email ID

            string reply_to = cn.reply_to;
            string display_name = cn.display_name;
            string subjectbody = cn.subject;
            string fromemailID = cn.email;
            string email_password = cn.email_password;
            string reply_display = cn.reply;
            string edm_subject = cn.edm_subject;
            /***********************************/
            //Load Server Details from config file
            string strsmtpclient = System.Configuration.ConfigurationSettings.AppSettings["SmtpClient"].ToString();
            int smtpport = Convert.ToInt16(ConfigurationSettings.AppSettings["SmtpPort"].ToString());
            int isSecured = Convert.ToInt16(ConfigurationSettings.AppSettings["isSecured"].ToString());
            string mailFrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
            string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();

            try
            {
                // invoke mongodb connection method in dal
                DataAccess dal = new DataAccess();
                emailusers usrdta = new emailusers();

                //Gettin EDM details
                string qry = "{'edmid': '" + convertToString(cn.edmid) + "'}";
                BsonDocument doc = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qry);
                QueryDocument qDoc = new QueryDocument(doc);
                string[] in_fields = new string[] { "user_collection", "edm_content" };
                string[] ex_fields = new string[] { "_id" };
                MongoCursor cursr = dal.execute_mongo_db("edm_master", qDoc, in_fields, ex_fields);
                MongoCursor cursor_unsub = dal.execute_mongo_db("unsubscribe");

                string email_body = "";
                string user_collection = "";

                bool flag = false;
                foreach (var c in cursr)
                {
                    user_collection = check_field(c.ToBsonDocument(), "user_collection") ? convertToString(c.ToBsonDocument()["user_collection"]) : null;
                    email_body = check_field(c.ToBsonDocument(), "edm_content") ? convertToString(c.ToBsonDocument()["edm_content"]) : null;
                    break;

                }

                //Getting Email IDs details to whom edms to be send out
                string qry1 = "{'edmid': '" + convertToString(cn.edmid) + "'}";
                BsonDocument doc1 = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qry1);
                QueryDocument qDoc1 = new QueryDocument(doc1);
                string[] in_fields1 = new string[] { "EmailID", "FirstName", "LastName", "Sno", "edmid" };
                string[] ex_fields1 = new string[] { "_id" };
                MongoCursor cursr1 = dal.execute_mongo_db(user_collection, qDoc1, in_fields1, ex_fields1);

                foreach (var c in cursr1)
                {
                    strTo = check_field(c.ToBsonDocument(), "EmailID") ? convertToString(c.ToBsonDocument()["EmailID"]) : "";
                    first_name = (check_field(c.ToBsonDocument(), "FirstName") ? convertToString(c.ToBsonDocument()["FirstName"]) : "").ToString();
                    last_name = (check_field(c.ToBsonDocument(), "LastName") ? convertToString(c.ToBsonDocument()["LastName"]) : "").ToString();
                    edm_id = (check_field(c.ToBsonDocument(), "edmid") ? convertToString(c.ToBsonDocument()["edmid"]) : "").ToString();
                    srlNo = check_field(c.ToBsonDocument(), "Sno") ? convertToString(c.ToBsonDocument()["Sno"]) : "";

                    subject = first_name + ", " + subjectbody;


                    /**Checking/Comparing with the whole Unsubscribe lists **/
                    foreach (var un in cursor_unsub)
                    {
                        user_email = check_field(un.ToBsonDocument(), "user_email") ? convertToString(un.ToBsonDocument()["user_email"]) : "";
                        user_email = user_email.Trim();

                        unChk = false;

                        if (strTo == user_email)
                        {
                            unChk = true;
                            break;
                        }
                    }

                    ecomm.model.repository.helper_repository hr = new ecomm.model.repository.helper_repository();

                    if (unChk == false)
                    {
                        int iresult = hr.SendMail(strsmtpclient.Trim(), fromemailID.Trim(), email_password, Convert.ToInt32(smtpport),
                           isSecured, display_name.Trim(), reply_to, reply_display, strTo, "", fromemailID.Trim(), subject.ToString(), email_body.ToString().Replace("^^", strTo));


                        if (iresult == 1)
                        {
                            strLog = "Sl No. " + srlNo + " Passed : " + strTo;
                            hr.Loginfo(strLog);
                        }
                        else
                        {
                            strLog = "Sl No. " + srlNo + " Failed : " + strTo;
                            hr.Loginfo(strLog);
                        }
                    }
                    else
                    {
                        strLog = "Sl No. " + srlNo + " Unsubscribed : " + strTo;
                        hr.Loginfo(strLog);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public List<edm> get_edm_name()
        {
            try
            {
                List<edm> olpm = new List<edm>();
                dal.DataAccess dal = new DataAccess();

                //****************************Get edm list****************************************//        

                string[] in_fields2 = new string[] { "edmid", "edmname" };
                string[] ex_fields2 = new string[] { "_id" };
                MongoCursor cursr2 = dal.execute_mongo_db("edm_master", in_fields2, ex_fields2);

                foreach (var c in cursr2)
                {
                    edm pm = new edm();
                    pm.edmid = check_field(c.ToBsonDocument(), "edmid") ? convertToString(c.ToBsonDocument()["edmid"]) : null;
                    pm.edmname = check_field(c.ToBsonDocument(), "edmname") ? convertToString(c.ToBsonDocument()["edmname"]) : null;
                    olpm.Add(pm);
                }
                return olpm;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return new List<edm>();
            }
        }


        //public List<grid> grid_invalid_mail(grid ieu)
        //{
        //    try
        //    {
        //        List<grid> olpm = new List<grid>();
        //        dal.DataAccess dal = new DataAccess();

        //        //****************************Grid Invalid email list****************************************//        

        //        string[] in_fields2 = new string[] { "edmid", "Sno", "FirstName", "LastName", "EmailID" };
        //        string[] ex_fields2 = new string[] { "_id" };              
        //        return olpm;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return new List<grid>();
        //    }



        //}


    }



}




