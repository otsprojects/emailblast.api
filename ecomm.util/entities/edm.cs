﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
namespace ecomm.util.entities
{
    public class edm
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string EDMFileLoc { get; set; }
        public string edmid { get; set; }
        public string edmname { get; set; }
        public string edm_desc { get; set; }
        public DateTime start_dt { get; set; }
        public DateTime end_dt { get; set; }
        public String edm_content { get; set; }
        public DateTime created_dt { get; set; }

    }

    public class config
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string edmid { get; set; }
        public string smtp_server { get; set; }
        public string smtp_port { get; set; }
        public string email { get; set; }
        public string display_name { get; set; }
        public string subject { get; set; }
        public string email_password { get; set; }
        public string reply { get; set; }
        public string reply_to { get; set; }
        public string edm_subject { get; set; }
        public string edm_server { get; set; }
        public DateTime created_dt { get; set; }

    }

    public class DocFile
    {
        public long RC_NO { get; set; }
        public string RC_FILE_NAME { get; set; }
        public string RC_FILE_GUID { get; set; }
        public string RC_FILE_PATH { get; set; }
        public DateTime UPLOAD_DATE { get; set; }
        public int STATUS { get; set; }
        public long COMP_ID { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string MODIFIED_BY { get; set; }
        public DateTime MODIFIED_DATE { get; set; }
        public long FLAG { get; set; }

    }
    public class admin_users
    {
        public string user_name { get; set; }
        public string password { get; set; }
        public string msg { get; set; }


    }

    public class import
    {
        public string _id { get; set; }
        public string csvid { get; set; }
        public string edmname { get; set; }
        public string myFile { get; set; }
        public string CsvFileLoc { get; set; }      
        public DateTime created_dt { get; set; }

    }

    public class emailusers
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string edmid { get; set; }
        public string Sno { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
    
    }

    public class grid
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string edmid { get; set; }
        public string Sno { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailID { get; set; }
    
    }

}
