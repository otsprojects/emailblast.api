﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class program_master
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public int program_id { get; set; }
        public string program_name { get; set; }
        public string program_desc { get; set; }
        public DateTime program_start_dt { get; set; }
        public DateTime program_end_dt { get; set; }
        public string created_by { get; set; }
        public string created_date { get; set; }
        public string modified_by { get; set; }
        public string modified_date { get; set; }
        public string ref_link { get; set; }
        public int active { get; set; }
    }

    public class sku_master
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string brand { get; set; }
        public string sku { get; set; }
        public string description { get; set; }
        public int active { get; set; }
        public int allotted_points { get; set; }
        public DateTime created_date { get; set; }

    }

    public class countercollection
    {
        public string _id { get; set; }
        public string seq { get; set; }
    }

    public class program_rule
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public int program_id { get; set; }
        public int program_rule_id { get; set; }
        public List<sku_rule> sku_rule { get; set; }
        public List<inv_dt_rule> inv_date_rule { get; set; }
        public string created_by { get; set; }
        public DateTime created_date { get; set; }
        public int active { get; set; }
        public string status { get; set; }
        public DateTime deactivate_date { get; set; }
    }

    public class sku_rule
    {
        public string sku { get; set; }
        public int allotted_points { get; set; }
    }

    public class inv_dt_rule
    {

        public int min_days { get; set; }
        public int max_days { get; set; }
        public int allotted_points { get; set; }
    }

}
