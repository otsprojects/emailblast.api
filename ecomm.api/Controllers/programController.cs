﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using ecomm.util.entities;
using System.Web;
using System.IO;


namespace ecomm.api.Controllers
{
    public class programController : ApiController
    {

        [POST("/program/postPopPrograms/")]
        public List<program_master> postPopPrograms(program_master opm)
        {
            ecomm.model.repository.program_repository hr = new ecomm.model.repository.program_repository();
            return hr.get_all_program_master(opm);
        }


        [POST("/program/postSKUProgramsRuleDetails/")]
        public List<sku_master> postSKUProgramsRuleDetails(program_master opm)
        {
            ecomm.model.repository.program_repository hr = new ecomm.model.repository.program_repository();
            return hr.get_sku_program_rule(opm);
        }



        [POST("/program/postSKUProgramsRule/")]
        public string postSKUProgramsRule(program_rule opr)
        {
            ecomm.model.repository.program_repository hr = new ecomm.model.repository.program_repository();
            return hr.save_sku_program_rule(opr);
        }



        [POST("/program/postInvDateProgramsRuleDetails/")]
        public List<program_rule> postInvDateProgramsRuleDetails(program_master opm)
        {
            ecomm.model.repository.program_repository hr = new ecomm.model.repository.program_repository();
            return hr.get_InvDate_program_rule(opm);
        }



        [POST("/program/postInvDateProgramsRule/")]
        public string postInvDateProgramsRule(program_rule opr)
        {
            ecomm.model.repository.program_repository hr = new ecomm.model.repository.program_repository();
            return hr.save_InvDate_program_rule(opr);
        }

    }
}
