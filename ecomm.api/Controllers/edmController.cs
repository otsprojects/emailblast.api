﻿using AttributeRouting.Web.Http;
using ecomm.model.repository;
using ecomm.util;
using ecomm.util.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace ecomm.api.Controllers
{
    public class edmController : ApiController
    {
        //for login page
        [POST("/edm/postlogin_user/")]
        public admin_users postlogin_user(admin_users sysuser)
        {
            ecomm.model.repository.edm_repository cr = new ecomm.model.repository.edm_repository();
            return cr.check_user_login(sysuser);           
        }

        //for data save from edm page or first(edm details) page
        [POST("/edm/DocFile/postpdfdoc/")]
        public string postpdfdoc()
        {
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                HttpResponseMessage result = null;
                var httpRequest = HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    var docfiles = new List<string>();
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var edm_data = httpRequest.Form["edm_data"];

                        edm eee = JsonConvert.DeserializeObject<edm>(edm_data.ToString());

                        string strFileName = string.Empty;

                        strFileName = postedFile.FileName;

                        string Rootpath = System.Configuration.ConfigurationManager.AppSettings["EDM_STORE"];

                        if (!System.IO.Directory.Exists(Rootpath))
                            System.IO.Directory.CreateDirectory(Rootpath);

                        var EDM_ID = "EDM" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();

                        Rootpath = Rootpath + "\\" + EDM_ID + "\\";
                        if (!System.IO.Directory.Exists(Rootpath))
                            System.IO.Directory.CreateDirectory(Rootpath);

                        string strEDMFileLoc = Rootpath + strFileName;
                        postedFile.SaveAs(strEDMFileLoc);
                        string html = File.ReadAllText(strEDMFileLoc).Replace("\r\n", "");
                        html = html.Replace("\"", "'");
                        eee.EDMFileLoc = strEDMFileLoc;

                        eee.edmid = EDM_ID;
                        

                        //string unsub_link = "http://unsubscribe.annectos.net/#/main/unsubscribe/" + EDM_ID;
                        string unsub_link = "http://unsubscribe.annectos.net/#/main/unsubscribe/" + "^^";
                        html = html.Replace("~", "\"");
                        html = html.Replace("**unsub_link**", unsub_link);

                        eee.edm_content = html;
                        


                        eee.created_dt = System.DateTime.Now.ToLocalTime();
                        ecomm.model.repository.edm_repository cr = new ecomm.model.repository.edm_repository();
                        return cr.store_edm_data(eee);
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#DEMO");
            }
            return "oc";
        }


        //for get dropdown value in import page
        [POST("/edm/postEDMlist/")]
        public List<edm> postEDMlist()
        {
            ecomm.model.repository.edm_repository er = new ecomm.model.repository.edm_repository();
            return er.get_edm_name();
        }

        //for data save from csv upload page
        [POST("/edm/DocFile/postcsvfile/")]
        public List<emailusers> postcsvfile()
        {
            List<OutCollection> cv = new List<OutCollection>();
            List<emailusers> oleu = new List<emailusers>();
            try
            {
                HttpResponseMessage result = null;
                var httpRequest = HttpContext.Current.Request;
                ecomm.model.repository.helper_repository hr = new ecomm.model.repository.helper_repository();

                if (httpRequest.Files.Count > 0)
                {
                    var docfiles = new List<string>();
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        var edm_id = httpRequest.Form["edm_id"];

                        string strFileName = string.Empty;

                        strFileName = postedFile.FileName;

                        string Rootpath = System.Configuration.ConfigurationManager.AppSettings["CSV_STORE"];

                        if (!System.IO.Directory.Exists(Rootpath))
                            System.IO.Directory.CreateDirectory(Rootpath);

                        var CSV_ID = "CSV" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                        Rootpath = Rootpath + "\\" + CSV_ID + "\\";
                        if (!System.IO.Directory.Exists(Rootpath))
                            System.IO.Directory.CreateDirectory(Rootpath);

                        string strCSVFileLoc = Rootpath + strFileName;
                        postedFile.SaveAs(strCSVFileLoc);
                        System.IO.StreamReader sr = new System.IO.StreamReader(strCSVFileLoc);
                        ecomm.model.repository.edm_repository crv = new ecomm.model.repository.edm_repository();
                        string strLine = "";
                        int i = 0;
                        string collection_name = "campaign-" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Second.ToString();
                        while (!sr.EndOfStream)
                        {
                            i += 1;
                            strLine = sr.ReadLine();
                            if (i > 1)
                            {
                                string[] strSplit = strLine.Split(';');
                                if (strSplit.Length >= 3)
                                {
                                    emailusers oeu = new emailusers();
                                    oeu.edmid = edm_id;
                                    oeu.Sno = strSplit[0];
                                    oeu.FirstName = strSplit[1];
                                    oeu.LastName = strSplit[2];
                                    oeu.EmailID = strSplit[3];
                                    string status = "";

                                    if (IsValidEmailId(oeu.EmailID))
                                    {
                                        status = "Valid";
                                        crv.save_upload_list(oeu, collection_name);
                                    }
                                    else
                                    {
                                       
                                        hr.Loginfo("Sl No. " + oeu.Sno + " InValid : " + oeu.EmailID);
                                        oleu.Add(oeu);
                                    }
                                }
                            }
                        }
                        string re = crv.update_edm_content(edm_id, collection_name);
                    }
                }
                return oleu;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#DEMO");
                return new List<emailusers>();
            }

        }
        /****Regex To validate Email Address****/
        private bool IsValidEmailId(string InputEmail)
        {
            //Regex To validate Email Address
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            Match match = regex.Match(InputEmail);
            if (match.Success)
                return true;
            else
                return false;
        }

        ////section for Upload CSV file
        //private void UploadCsvFile()
        //{
        //    edm emailusers;

        //    string strLine = "";
        //    int i = 0;

        //    string Sno = "";
        //    string FirstName = "";
        //    string LastName = "";
        //    string EmailID = "";

        //    System.IO.StreamReader cr = new System.IO.StreamReader(txtFile.Text);

        //    while (!cr.EndOfStream)
        //    {
        //        i += 1;
        //        strLine = cr.ReadLine();

        //        if (i > 1)
        //        {
        //            string[] strSplit = strLine.Split(';');
        //            if (strSplit.Length >= 3)
        //            {
        //                Sno = strSplit[0];
        //                FirstName = strSplit[1];
        //                LastName = strSplit[2];
        //                EmailID = strSplit[3];


        //                //addtoDataTable(Sno, FirstName, LastName, EmailID);
        //            }
        //        }
        //    }

        //}



        [POST("/edm/postListUpload/")]
        //public string postListUpload(import li)
        //{
        //    ecomm.model.repository.edm_repository ul = new ecomm.model.repository.edm_repository();
        //    return ul.save_upload_list(li);
        //}

        [POST("/edm/postConfigureData/")]
        public string postConfigureData(config cn)
        {
            ecomm.model.repository.edm_repository cl = new ecomm.model.repository.edm_repository();
            return cl.save_upload_cnfgr(cn);


        }
        [POST("/edm/postemaildata/")]
        public void postemaildata(config cn)
        {
            ecomm.model.repository.edm_repository cl = new ecomm.model.repository.edm_repository();
            cl.send_mail(cn);


        }
    }
}



